git init													//iniciar el repositorio de forma local
git remote add origin gitlab.com:GroupName/ProjectName.git	//se agrega de forma remota el repositorio

git branch                                      //saber en que rama estas ubicado
git branch -a                                   //saber en que rama estas ubicado y las ramas remotas
git checkout -b feature_x                       //Crea una nueva rama llamada "feature_x" y cámbiate a ella usando
git checkout master                             //vuelve a la rama principal
git branch -d feature_x                         //y borra la rama
git push origin <branch>                        //Una rama nueva no estará disponible para los demás a menos que subas (push) la rama a tu repositorio remoto
git fetch origin                                //actualiza tus referencias remotas
git push origin --delete <NombreRama>           //puede eliminar una rama remota
git branch -d <NombreRama>              	     	//puede elimar una rama local


git add .                                       //se agregan los cambios al git en local sin subirlos a remoto
git commit -m "Comentario"                      //se agrega un comentario local sin subir todavia la info a remoto
git push -u origin <NombreRama>                 //se suben los cambios al rpositorio remoto     // recuerda cambiar la rama master por la respectiva que estes trabajando
git pull origin <NombreRama>                    //se descargan los cambios del repositorio remoto al local //recuerda cambiar la rama master por la rama develop que es la que esta mas actualizada

git status                                      //Para conocer el estado de nuestros archivos
git log –-oneline                                //verificar los commits realizados anteriormente
git checkout id_commit                          //vuelve a un punto anterior de los commit especificado por el id
git push origin +master                         //fuerza un push
